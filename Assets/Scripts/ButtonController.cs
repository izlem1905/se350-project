﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonController : MonoSingleton<ButtonController>
{
    [SerializeField] private Button B1, B2, B3, B4;
    [SerializeField] private CharacterHandler characterHandler;

    public void Move(int XPos)
    {
        DisableAllButtons();
        characterHandler.Move(XPos);
    }
    public void DisableAllButtons()
    {
        B1.enabled = false;
        B2.enabled = false;
        B3.enabled = false;
        B4.enabled = false;
        B1.transform.DOScale(0f, 0.4f);
        B2.transform.DOScale(0f, 0.4f);
        B3.transform.DOScale(0f, 0.4f);
        B4.transform.DOScale(0f, 0.4f);


    }
    public void EnableAllButtons()
    {
        B1.enabled = true;
        B2.enabled = true;
        B3.enabled = true;
        B4.enabled = true;
        B1.transform.DOScale(1f, 0.4f);
        B2.transform.DOScale(1f, 0.4f);
        B3.transform.DOScale(1f, 0.4f);
        B4.transform.DOScale(1f, 0.4f);
    }
}
