﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CharacterHandler : MonoBehaviour
{
    private int columnCount;

    private void Inıtılaize()
    {
        foreach (var item in GameManager.Instance.HumanGrids[columnCount])
        {
            item.CloseCodeText();
        }
    }

    public void Move(int XPos)
    {
        var orginalPos = transform.localPosition;

        var newPos = new Vector3(XPos + 1f, transform.localPosition.y, transform.localPosition.z + 2f);
        transform.DOLocalJump(newPos, 2f, 1, 1f).OnComplete(() =>
          {
              if (ControlGrid(XPos))
              {
                  GameManager.Instance.HumanTurn = true;
                  foreach (var item in GameManager.Instance.HumanGrids[columnCount])
                  {
                      item.CloseCodeText();
                  }
                  columnCount++;
                  foreach (var item in GameManager.Instance.HumanGrids[columnCount])
                  {
                      item.OpenCodeText();
                  }
                  ButtonController.Instance.EnableAllButtons();
              }
              else
              {
                  BackToOrginalPos(orginalPos);
              }
          });
    }

    private void BackToOrginalPos(Vector3 orginalPos)
    {
        transform.DOLocalJump(orginalPos, 2f, 1, 1f).OnComplete(() =>
           {
               GameManager.Instance.HumanTurn = false;
           });
    }

    private bool ControlGrid(int XPos)
    {
        return XPos == GameManager.Instance.HumanControlCodes[columnCount];
    }
}
