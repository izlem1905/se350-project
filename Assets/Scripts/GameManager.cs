﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameStates
{
    MainMenu,
    Gameplay,
    GameOver,
    Transmision,
    LevelCompleted
}
public class GameManager : MonoSingleton<GameManager>
{

    public static UnityAction OnGameStarted, OnGameSceneLoad, OnTrasmission;
    public static UnityAction OnGameOver, OnLevelComplete;
    public static UnityAction<GameStates> OnGameStateChange;

    private GameStates currentState = GameStates.MainMenu;
    public GameStates CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            currentState = value;
            OnGameStateChange?.Invoke(value);
        }
    }

    public bool HumanTurn;
    public List<int> HumanControlCodes = new List<int>();

    public List<List<Grid>> HumanGrids = new List<List<Grid>>();
    private List<Grid> AIGrids = new List<Grid>();




    private void Start()
    {
        HumanControlCodes = CreateGrid();
        HumanTurn = true;
    }

    private List<int> CreateGrid()
    {

        return GridSpawner.Instance.CretaGrid();
    }

    public void PlayButton()
    {
        OnTrasmission?.Invoke();
        CurrentState = GameStates.Transmision;
        Invoke("StartGame", 1f);
    }
    public void StartGame()
    {
        OnGameStarted?.Invoke();
        CurrentState = GameStates.Gameplay;
    }
}
