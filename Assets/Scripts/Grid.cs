﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Grid : MonoBehaviour
{
    public GridType GridType;

    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private TextMeshPro codeText;

    private Material ownMaterial;

    public void Initialize(Material material, int code)
    {
        ownMaterial = material;
        codeText.text = code.ToString();
        codeText.transform.localScale = Vector3.zero;
    }
    public void OpenCodeText()
    {
        codeText.transform.DOScale(1f, 0.4f);
    }
    public void CloseCodeText()
    {
        codeText.transform.DOScale(0f, 0.4f);
    }
}
