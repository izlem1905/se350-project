﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GridType
{
    Correct,
    Wrong
}
public class GridSpawner : MonoSingleton<GridSpawner>
{
    [SerializeField] private Grid gridTemplate;

    [SerializeField] private float XDistance, ZDistance;
    [SerializeField] private int rowCount, columnCount;

    [SerializeField] private Material correctGridMaterial;
    [SerializeField] private Material wrongGridMaterial;


    private List<int> newGridMaps;

    private float currentXDistance;
    private float currentZDistance;

    public List<int> CretaGrid()
    {

        newGridMaps = new List<int>();
        for (int i = 0; i < columnCount; i++)
        {
            currentZDistance += ZDistance;
            currentXDistance = 0;
            var newGridList = new List<Grid>();
            var correctGrid = Random.Range(0, rowCount);

            for (int j = 0; j < rowCount; j++)
            {
                currentXDistance += XDistance;

                var pos = new Vector3(currentXDistance, 0f, currentZDistance);
                var grid = Instantiate(gridTemplate, pos, Quaternion.identity);
                if (correctGrid == j)
                {

                    newGridMaps.Add((int)currentXDistance);
                    grid.GridType = GridType.Correct;
                    grid.Initialize(correctGridMaterial, j + 1);
                }
                else
                {
                    grid.GridType = GridType.Wrong;
                    grid.Initialize(wrongGridMaterial, j + 1);
                }
                newGridList.Add(grid);
                grid.transform.localPosition = pos;
            }

            GameManager.Instance.HumanGrids.Add(newGridList);
        }
        return newGridMaps;
    }
}
