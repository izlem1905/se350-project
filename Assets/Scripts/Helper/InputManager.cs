﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoSingleton<InputManager>
{

    public float FlickAngle => angle;

    public bool FlickStarted => flickStarted;
    public bool FlickEnded => flickEnded;

    private Vector2 previusMousePos;
    private Vector2 currentDirection;

    private float angle;
    private bool screenTouch;


    private bool flickStarted;
    private bool flickEnded;

    private void Update()
    {
        FlickHandle();
        //if (GameManager.Instance.CurrentState == GameStates.Gameplay)
        //{
        //    FlickHandle();
        //}
    }

    private void FlickHandle()
    {

        flickEnded = false;
        if (Input.GetMouseButtonDown(0))
        {
            angle = 0;
            screenTouch = true;
            previusMousePos = Input.mousePosition;
            flickStarted = true;
            flickEnded = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (flickStarted)
            {
                var mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                currentDirection = mousePos - previusMousePos;
                angle = Vector2.Angle(currentDirection, Vector2.right);
                screenTouch = false;
                previusMousePos = Vector2.zero;
                flickStarted = false;
                flickEnded = true;
            }
        }
    }

}
