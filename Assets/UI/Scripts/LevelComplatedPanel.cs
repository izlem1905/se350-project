﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelComplatedPanel : MonoBehaviour
{
    public void NextLevel()
    {
        SceneManager.LoadScene("GameScene");
    }
}
