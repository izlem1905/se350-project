﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private MainMenuPanel mainMenuPanel;
    [SerializeField] private GamePlayPanel gamePlayPanel;
    [SerializeField] private LevelComplatedPanel levelComplatedPanel;
    [SerializeField] private GameOverPanel gameOverPanel;

    private float panelFadeTime = 0.5f;
    private GameObject currentPanel;
    private void Start()
    {
        CloseAllPanel();
        OpenPanel(mainMenuPanel.gameObject);
    }

    private void OnEnable()
    {
        GameManager.OnGameStateChange += ChangePanel;
    }

    private void OnDisable()
    {
        GameManager.OnGameStateChange -= ChangePanel;

    }

    private void CloseAllPanel()
    {

        ClosePanel(mainMenuPanel.gameObject);
        ClosePanel(gamePlayPanel.gameObject);
        ClosePanel(levelComplatedPanel.gameObject);
        ClosePanel(gameOverPanel.gameObject);

    }

    private void ChangePanel(GameStates gameStates)
    {
        GameObject newPanel = null;
        bool withAnimated = false;
        switch (gameStates)
        {
            case GameStates.MainMenu:
                withAnimated = true;
                newPanel = mainMenuPanel.gameObject;
                break;
            case GameStates.Transmision:
                withAnimated = true;
                newPanel = gamePlayPanel.gameObject;
                break;
            case GameStates.Gameplay:
                withAnimated = false;
                newPanel = gamePlayPanel.gameObject;
                break;
            case GameStates.GameOver:
                withAnimated = true;
                newPanel = gameOverPanel.gameObject;
                break;
            case GameStates.LevelCompleted:
                withAnimated = true;
                newPanel = levelComplatedPanel.gameObject;
                break;
        }
        if (withAnimated)
        {
            OpenPanel(newPanel);

        }
    }

    private void OpenPanel(GameObject newPanel)
    {
        if (currentPanel != null)
        {
            currentPanel.GetComponent<CanvasGroup>().DOFade(0f, panelFadeTime).SetEase(Ease.Linear).OnComplete(() =>
            {
                currentPanel.SetActive(false);
                newPanel.SetActive(true);
                newPanel.GetComponent<CanvasGroup>().DOFade(1f, panelFadeTime).SetEase(Ease.Linear).OnComplete(() =>
                {

                    currentPanel = newPanel;
                });

            });
        }
        else
        {
            newPanel.SetActive(true);
            newPanel.GetComponent<CanvasGroup>().DOFade(1f, panelFadeTime).SetEase(Ease.Linear).OnComplete(() =>
            {

                currentPanel = newPanel;
            });
        }
    }

    private void ClosePanel(GameObject panel)
    {
        panel.GetComponent<CanvasGroup>().alpha = 0f;
        panel.SetActive(false);
    }




}
